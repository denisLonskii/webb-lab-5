import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ColumnsModule } from './columns/columns.module';
import { CardsModule } from './cards/cards.module';
import { Column } from './columns/entities/column.entity';
import { Card } from './cards/entities/card.entity';
import { TypeOrmModule } from '@nestjs/typeorm';


@Module({
  imports: [ColumnsModule, 
    CardsModule,
    TypeOrmModule.forRoot({
      type: 'postgres',
      url: 'postgres://hntwjcmb:ACjzzfbB00nkYM1UiEsx_xMPpzFs_NHZ@rosie.db.elephantsql.com/hntwjcmb',
      entities: [Card, Column],
      synchronize: true,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
