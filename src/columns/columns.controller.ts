import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Patch,
    Post,
  } from '@nestjs/common';
  import { ColumnsService } from './columns.service';
  import { CreateColumnDto } from './dto/create-column.dto';
  import { UpdateColumnDto } from './dto/update-column.dto';
  import { Column } from './entities/column.entity';
  import { CardsService } from '../cards/cards.service';
  import { Card } from '../cards/entities/card.entity';

  @Controller('columns')
  export class ColumnsController {
    constructor(
      private readonly columnsService: ColumnsService, 
      private readonly cardsService: CardsService
      ) {}
  
    @Get()
    getAll(): Promise<Column[]> {
      return this.columnsService.getAllColumns();
    }

    @Get(':id')
    getOne(@Param('id') id: Column['id']): Promise<Column> {
      return this.columnsService.getOneColumn(id);
    }
  
    @Post()
    createColumn(@Body() dto: CreateColumnDto): Promise<Column> {
      return this.columnsService.createColumn(dto);
    }
  
    @Patch(':id')
    updateColumn(
      @Param('id') id: Column['id'],
      @Body() dto: UpdateColumnDto,
    ): Promise<Column> {
      return this.columnsService.updateColumn(id, dto);
    }
  
    @Delete(':id')
    deleteColumn(@Param('id') id: Column['id']) {
      return this.columnsService.deleteColumn(id);
    }

    @Get(':id/cards')
    getByColumnId(@Param('id') columnId: string): Promise<Card[]> {
      return this.cardsService.getByColumnId(columnId);
    }
  
  }
