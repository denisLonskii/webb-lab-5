import { BaseEntity } from 'src/common/entities/base.entity';
import { 
  Entity, 
  PrimaryGeneratedColumn, 
  Column as ColumnName, 
  OneToMany } from 'typeorm';
import { Card } from 'src/cards/entities/card.entity';

const tableName = 'columns'

@Entity({ name: tableName })
export class Column extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ColumnName('varchar')
  name: string;

  @OneToMany(() => Card, card => card.column, { cascade: true}) 
  cards?: Card[]
}
