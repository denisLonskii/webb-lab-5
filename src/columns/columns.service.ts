import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from 'typeorm';
import { CreateColumnDto } from "./dto/create-column.dto";
import { UpdateColumnDto } from "./dto/update-column.dto";
import { Column } from "./entities/column.entity";



@Injectable()
export class ColumnsService {
  constructor(
    @InjectRepository(Column)
    private columnsRepository: Repository<Column>,
    ) {}
    

  async createColumn(dto: CreateColumnDto): Promise<Column> {
    return await this.columnsRepository.save(dto);
  }

  async getAllColumns(): Promise<Column[]> {
    return  await this.columnsRepository.find();
  }

  async getOneColumn(id: string): Promise<Column> {
    return await this.columnsRepository.findOne({id});
  }

  async updateColumn(id: string, dto: UpdateColumnDto): Promise<Column> {
    return await this.columnsRepository.save({id, ...dto});
  }

  async deleteColumn(id: string) {
    const column = await this.getOneColumn(id);
    return await this.columnsRepository.remove(column);
  }
}
