import { EntityRepository, Repository } from "typeorm";
import { Card } from "./entities/card.entity"

@EntityRepository(Card)
export class CardsRepository extends Repository<Card> {
    async getByColumnId(id: string): Promise<Card[]> {
        return await this.find({ where: { columnId: id } });
      }
}
