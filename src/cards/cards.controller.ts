import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Patch,
    Post,
  } from '@nestjs/common';
  import { CardsService } from './cards.service';
  import { CreateCardDto } from './dto/create-card.dto';
  import { UpdateCardDto } from './dto/update-card.dto';
  import { Card } from './entities/card.entity';

  @Controller('cards')
  export class CardsController {
    constructor(private readonly cardsService: CardsService) {}
  
    @Get()
    getAll(): Promise<Card[]> {
      return this.cardsService.getAllCards();
    }
  
    @Get(':id')
    getOne(@Param('id') id: Card['id']): Promise<Card> {
      return this.cardsService.getOneCard(id);
    }
    
    @Post()
    createCard(@Body() dto: CreateCardDto): Promise<Card> {
      return this.cardsService.createCard(dto);
    }
  
    @Patch(':id')
    updateCard(
      @Param('id') id: Card['id'],
      @Body() dto: UpdateCardDto,
    ): Promise<Card> {
      return this.cardsService.updateCard(id, dto);
    }
  
    @Delete(':id')
    deleteCard(@Param('id') id: Card['id']) {
      return this.cardsService.deleteCard(id);
    }
  }
