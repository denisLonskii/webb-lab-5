import { Injectable } from '@nestjs/common'
import { CardsRepository} from './cards.repository';
import { CreateCardDto } from './dto/create-card.dto';
import { UpdateCardDto } from './dto/update-card.dto';
import { Card } from './entities/card.entity'


@Injectable()
export class CardsService {
    constructor(private readonly cardsRepository: CardsRepository) {}

    async getAllCards(): Promise<Card[]> {
        return await this.cardsRepository.find();
    }

    async getOneCard(id: Card['id']): Promise<Card> {
        return await this.cardsRepository.findOne(id);
    }

    async createCard(dto: CreateCardDto): Promise<Card> {
        return await this.cardsRepository.save(dto);
    }

    async updateCard(id: Card['id'], dto: UpdateCardDto): Promise<Card> {
        return await this.cardsRepository.save({id, ...dto});
    }

    async deleteCard(id: Card['id']) {
        const card = await this.getOneCard(id);
        return await this.cardsRepository.remove(card);
    }

    async getByColumnId(columnId: string): Promise<Card[]> {
        return await this.cardsRepository.getByColumnId(columnId);
    }
    
}
