import { Column } from 'src/columns/entities/column.entity';
import { BaseEntity } from 'src/common/entities/base.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column as ColumnName,
  ManyToOne,
  JoinColumn
} from 'typeorm';


const tableName = 'cards';

@Entity({ name: tableName})
export class Card extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  
  @ColumnName('varchar')
  columnId: Column['id'];

  @ColumnName('varchar')
  name: string;

  @ManyToOne(() => Column, column => column.cards, {onDelete: 'CASCADE'})
  @JoinColumn({ name: 'columnId'})
  column: Column

}
